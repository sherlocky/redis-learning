package com.sherlocky.learning.redisson.utils;

import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.redisson.api.RBucket;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.redisson.config.SingleServerConfig;

public class RedissonUtilsTest {
	@Test
	public void test() throws InterruptedException {
		// redisson配置
		Config config = new Config();
		SingleServerConfig singleSerververConfig = config.useSingleServer();
		singleSerververConfig.setAddress("192.168.12.219:6379");
		singleSerververConfig.setPassword("lezhixing!@#$1qaz@WSX");
		// redisson客户端
		RedissonClient redissonClient = RedissonUtils.getInstance().getRedisson(config);
		RBucket<Object> rBucket = RedissonUtils.getInstance().getRBucket(redissonClient, "00:redisson:key");
		
		// rBucket.set("yyuyyuyuyyu");
		
		System.out.println(rBucket.get());
		while (true) {
			RLock lock = redissonClient.getLock("00:redisson:locks");
			lock.tryLock(0, 1, TimeUnit.SECONDS);// 第一个参数代表等待时间，第二是代表超过时间释放锁，第三个代表设置的时间制
			try {
				System.out.println("执行");
			} finally {
				lock.unlock();
			}
		}
	}
}
