package com.sherlocky.redisinaction;

import java.io.IOException;

import org.junit.Test;

public class ChaptersTest {
	
	@Test
	public void testChapter01() {
        new Chapter01().run();
	}
	
	@Test
	public void testChapter02() {
        try {
			new Chapter02().run();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testChapter04() {
		new Chapter04().run();
	}
	
	@Test
	public void testChapter05() {
        try {
			new Chapter05().run();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}		
	}
	
	@Test
	public void testChapter06() {
        try {
			new Chapter06().run();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testChapter07() {
        new Chapter07().run();
	}
	
	@Test
	public void testChapter08() {
        try {
			new Chapter08().run();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testChapter09() {
        new Chapter09().run();
	}
}
